package com.squedgy.mcmodmanager;

import com.squedgy.mcmodmanager.display.MainController;
import com.squedgy.mcmodmanager.util.PathUtils;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.squedgy.mcmodmanager.config.Configuration.getConfiguration;
import static org.slf4j.LoggerFactory.getLogger;

public class App /*extends Application*/ {

	private static final Logger log = getLogger(App.class);

	private static Scene PARENT;
	private static App instance;
	private static MainController MAIN_VIEW;

	private App() throws IOException {
		if (MAIN_VIEW == null) MAIN_VIEW = MainController.getInstance();
		if (PARENT == null) PARENT = new Scene(MAIN_VIEW.getRoot());
	}

	public static App getInstance() throws IOException {
		if (instance == null) instance = new App();
		return instance;
	}

	public static Scene getParent() {
		return PARENT;
	}

	public MainController getMainView() {
		return MAIN_VIEW;
	}

	private static void displaySetWindow() {
		Platform.runLater(() -> {
			try {
				Stage s = new Stage();
				Label l = new Label("Choose a Minecraft Version: ");
				ChoiceBox<String> choices = new ChoiceBox<>(FXCollections.observableArrayList(PathUtils.getPossibleMinecraftVersions()));
				HBox start = new HBox(l, choices);
				start.setAlignment(Pos.CENTER);
				start.setPadding(new Insets(5, 0, 5, 0));
				Button b = new Button("Set");
				HBox button = new HBox(b);
				button.setPadding(new Insets(5, 0, 5, 0));
				button.setAlignment(Pos.CENTER_RIGHT);
				Scene sc = new Scene(new VBox(start, button));
				b.setOnMouseClicked(e -> {
					log.info("setting minecraft version: {}", choices.getValue());
					getConfiguration().setMinecraftVersion(choices.getValue());
					getConfiguration().writeProps();
					s.close();
				});
				sc.getStylesheets().setAll(MainController.class.getResource("main.css").toString());
				s.setScene(sc);
				s.minHeightProperty().bind(new SimpleDoubleProperty(105));
				s.minWidthProperty().bind(new SimpleDoubleProperty(300));
				s.maxWidthProperty().bind(new SimpleDoubleProperty(300));
				s.maxHeightProperty().bind(new SimpleDoubleProperty(105));
				s.initOwner(App.getParent().getWindow());
				s.initModality(Modality.WINDOW_MODAL);
				s.setAlwaysOnTop(true);
				log.info("Show and wait happening");
				s.showAndWait();
			} catch (Exception e) {
				log.error("Failed to set minecraft version", e);
				System.exit(1);
			}
		});
	}

	public static void decideMinecraftVersion() {
		log.info("Configuration minecraftVersion: {}", getConfiguration().getMinecraftVersion());
		if (getConfiguration().getMinecraftVersion() == null) {
			log.info("Null Minecraft Version");
			displaySetWindow();
			while (getConfiguration().getMinecraftVersion() == null) {
				try {
					TimeUnit.MILLISECONDS.sleep(500);
				} catch (InterruptedException e) {
					log.error("", e);
				}
			}
		}
		getConfiguration().writeProps();
	}

	// @Override
	// public void start(Stage stage) throws IOException {

	// 	getInstance();
	// 	stage.setTitle("Minecraft Mod Manager");
	// 	stage.setMinHeight(MAIN_VIEW.getRoot().getMinHeight());
	// 	stage.setMinWidth(MAIN_VIEW.getRoot().getMinWidth());
	// 	stage.setScene(PARENT);
	// 	stage.onCloseRequestProperty().setValue(e -> {
	// 		System.exit(0);
	// 	});
	// 	stage.centerOnScreen();
	// 	stage.show();
	// }

}
