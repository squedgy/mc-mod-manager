package com.squedgy.mcmodmanager.controllers;

import com.squedgy.mcmodmanager.components.DisplayVersion;
import com.squedgy.mcmodmanager.display.ImageUtils;
import com.squedgy.mcmodmanager.display.JavafxUtils;
import com.squedgy.mcmodmanager.display.ModVersionTableController;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.App;
import com.squedgy.mcmodmanager.components.Modal;
import com.squedgy.mcmodmanager.thread.ModUpdaterThread;
import com.squedgy.mcmodmanager.util.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import static com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository.REPOSITORY_ID;
import static org.slf4j.LoggerFactory.getLogger;

public class ModUpdaterController {

	private static final Logger log = getLogger(ModUpdaterController.class);
	private static final String TABLE_NAME = "updater";
	@FXML
	public VBox root;
	@FXML
	public TilePane buttons;
	@FXML
	private Button updateAll;
	private static ModVersionTableController table;
	private static ModUpdaterThread updates;

	public ModUpdaterController(List<CurseForgeModDownloadInfo> updates) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("updates.fxml"));
		loader.setController(this);
		loader.load();
		table = new ModVersionTableController(TABLE_NAME, updates.toArray(new CurseForgeModDownloadInfo[0]));
		root.getChildren().add(0, table.getRoot());
		VBox.setVgrow(table.getRoot(), Priority.ALWAYS);
		updateAll.setVisible(table.getItems().size() > 0);
		HBox box = new HBox();
		Button button = new Button("Update All");
		button.onMouseReleasedProperty().setValue(this::updateAll);
		box.getChildren().setAll(button);
		box.setVisible(updates.size() > 0);
		Modal.getInstance().setFooter(box);
	}

	@FXML
	public void close(Event e){
		try {
			Modal.getInstance().close();
		} catch (IOException e1) {
			log.error(e1.getMessage(), getClass());
		}
	}

	public static void updateAll(List<CurseForgeModDownloadInfo> items){
		Platform.runLater(() ->{
			try {
				Modal modal = Modal.loading();
				if (updates == null || !updates.isAlive()) {
					updates = new ModUpdaterThread(
						items,
						results -> {
							TableView<Map.Entry<CurseForgeModDownloadInfo, Result>> resultTable = getResultTable();
							System.gc();
							Platform.runLater(() -> {
								results.forEach((key, value) -> {
									if (value.isResult()) handleSuccessfulDownload(key, value);
									resultTable.getItems().add(new AbstractMap.SimpleEntry<>(key, value));
								});
								modal.setContent(resultTable);
								try {
									NewModUtils.getInstance().loadMods();
									App.getInstance().getMainView().updateModList();
								} catch (IOException e2) {
									log.error("", e2);
								}
							});
							return null;
						}
					);

				}
				if (!updates.isAlive()) updates.start();
			} catch (IOException e1) {
				log.error("", e1);
			}
		} );
	}

	@FXML
	private void updateAll(Event e)  {
		updateAll(
			table.getItems()
			     .stream()
				 .filter(v -> v.getRepositoryId().equals(REPOSITORY_ID))
				 .map(DisplayVersion::toCurseInfo)
			     .filter(Objects::nonNull)
			     .collect(Collectors.toList()));
	}

	private static void handleSuccessfulDownload(CurseForgeModDownloadInfo key, Result value) {
		CurseForgeModDownloadInfo old =
			(CurseForgeModDownloadInfo) NewModUtils.getInstance().cache.getItem(key.getRepositoryUniqueId());


		File newMod = new File(PathUtils.findModLocation(key));
		File oldMod = new File(PathUtils.findModLocation(old));

		log.info("Old File ||| New File\n" + oldMod.getAbsolutePath() + "|||" + newMod.getAbsolutePath(), ModUpdaterController.class);

		value.setResult(oldMod.delete());
		value.setReason(value.isResult() ? "Succeeded!" : "couldn't delete the old file");
		if (!value.isResult()) {
			if (newMod.delete()) {
				value.setReason("Couldn't locate/delete previous file, deleted new one to ensure runnability of MC");
			} else {
				value.setReason("Couldn't delete the new file after not locating/deleting the old.\n" +
					"You should delete " + oldMod.getAbsolutePath() + " to have no issues AND keep the new mod.\n" +
					"Otherwise delete " + newMod.getAbsolutePath() + " to keep the old version.");
			}
		}

		if (value.isResult()) {
			String jarId;
			try {
				jarId = NewModUtils.getInstance().getJarModId(new JarFile(newMod));
			} catch (IOException e1) {
				jarId = old.getRepositoryUniqueId();
			}
			try {
				NewModUtils.getInstance().addMod(jarId, key, true);
			} catch(IOException e) {
				log.error("Failed to add mod {}", key.getRepositoryUniqueId(), e);
			}
		}
	}

	private static TableView<Map.Entry<CurseForgeModDownloadInfo, Result>> getResultTable() {
		TableView<Map.Entry<CurseForgeModDownloadInfo, Result>> ret = new TableView<>();
		ret.getStyleClass().addAll("all-padding", "mod-table");
		TableColumn<Map.Entry<CurseForgeModDownloadInfo, Result>, ImageView>
			image = JavafxUtils.makeColumn("Succeeded", e -> {
				ImageUtils u = ImageUtils.getInstance();
				boolean succeed = e.getValue().getValue().isResult();
				return new SimpleObjectProperty<>(new ImageView(succeed ? u.GOOD : u.BAD));
			}
		);
		TableColumn<Map.Entry<CurseForgeModDownloadInfo, Result>, String>
			mod = JavafxUtils.makeColumn("Mod", e -> new SimpleStringProperty(e.getValue().getKey().getDisplayName())),
			reason = JavafxUtils.makeColumn("Reason", e -> {
				boolean succeed = e.getValue().getValue().isResult();
				return new SimpleStringProperty(succeed ? "Succeeded" : e.getValue().getValue().getReason());
			}
		);

		ret.getColumns().addAll(
			image,
			mod,
			reason
		);

		return ret;
	}

	public VBox getRoot() {
		return root;
	}

}
