package com.squedgy.mcmodmanager.controllers;

import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.request.VersionedRequest;
import com.squedgy.mcmodmanager.response.ModIdNotFoundException;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.components.Modal;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import com.squedgy.mcmodmanager.util.PathUtils;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;

import static com.squedgy.mcmodmanager.config.Configuration.getConfiguration;
import static org.slf4j.LoggerFactory.getLogger;

public class NewCurseForgeModController {

	private static final Logger log = getLogger(NewCurseForgeModController.class);
	private static final Configuration CONFIG = getConfiguration();
	@FXML
	private VBox root;
	@FXML
	private TextArea mods;
	@FXML
	private Label ids;
	@FXML
	private HBox box;
	private static HBox footer;

	public NewCurseForgeModController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("curseForgeMods.fxml"));
		loader.setController(this);
		loader.load();
		if(footer == null) {
			HBox box = new HBox();
			Button b = new Button("Add Mods");
			b.onMouseReleasedProperty().setValue(this::addMods);
			box.getChildren().setAll(b);
			footer = box;
		}
		box.prefHeightProperty().bind(root.heightProperty().subtract(ids.heightProperty()));
		Modal.getInstance().setFooter(footer);
	}

	public VBox getRoot() {
		return root;
	}

	private boolean downloadMod(CurseForgeModDownloadInfo v, String output, NewModUtils utils) {
		try (
			FileOutputStream outFile = new FileOutputStream(output);
			ReadableByteChannel in = Channels.newChannel(CurseForgeRepository.getInstance().download(v));
			FileChannel out = outFile.getChannel()
		) {
			out.transferFrom(in, 0, Long.MAX_VALUE);
			utils.addMod(NewModUtils.getInstance().getJarModId(new JarFile(PathUtils.getModsDir() + File.separator + v.getFileName())), v, "", true);
			return true;
		} catch (ModIdNotFoundException | IOException | NullPointerException ignored) {
			return false;
		}
	}

	@FXML
	public void addMods(Event e)  {
		//A mod id consists of "-" and alpha-numerical, things that aren't those are the delimiters
		try { Modal.loading(); }
		catch (IOException e1) { log.error(e1.getMessage(), getClass()); }
		Thread t = new Thread(() -> {

			List<ModInfo> updates = new LinkedList<>();
			NewModUtils utils = NewModUtils.getInstance();
			Map<String, Result> results = new HashMap<>();

			for (String id : mods.getText().split("([^-a-zA-Z0-9]|[\r\n])+")) {
				//Check normal and deactivated mods
				ModInfo mod = utils.getAnyModFromId(id);
				Result result = new Result();
				result.succeded = false;

				if (mod == null) {
					try {
						checkCurseForge(id, result);
					} catch (Exception ex) {
						log.error("", ex);
						result.reason = "Failed to locate a mod with id: " + id;
					}
				} else {
					updates.add(mod);
					result.reason = "Mod already exists, currently attempting to update";
				}
				results.put(id, result);
			}
			try {
//				ModUpdaterController.updateAll(updates);
				afterAdd(results);
			} catch (IOException ignored) {
			}
			e.consume();
		});
		t.start();
	}

	private void afterAdd(Map<String, Result> results) throws IOException {
		Modal m = Modal.getInstance();
		VBox resultBox = new VBox();
		results.forEach((key, value) -> resultBox.getChildren().add(new Label(key + ": " + (value.succeded ? "Succeeded!!" : value.reason))));
		Button b = new Button("Add More Mods");
		b.setOnMouseReleased(event -> {
			Platform.runLater(() -> {
				mods.clear();
				m.setContent(root);
				m.setFooter(footer);
			});
		});
		Platform.runLater(() ->{
			m.setContent(resultBox);
			m.setFooter(new HBox(b));
		});
	}

	private Result checkCurseForge(String id, Result result) throws ModIdNotFoundException, Exception {
		NewModUtils utils = NewModUtils.getInstance();
		CurseForgeModDownloadInfo mod = CurseForgeRepository.getInstance().getLatest(new VersionedRequest(id, CONFIG.getMinecraftVersion()));
		ModInfo current = utils.cache.getItem(id);
		if(current != null && mod.getFileName().equals(current.getFileName())){
			result.reason = "Already have the latest version.";
			result.succeded = true;
			result.version = mod;
		} else if (downloadMod(mod, PathUtils.getModLocation(mod), utils)) {
			result.succeded = true;
			result.version = mod;
		} else {
			result.reason = "Failed to download: " + mod.getDisplayName();
		}
		return result;
	}

	private static class Result {
		private CurseForgeModDownloadInfo version;
		private boolean succeded;
		private String reason;
	}

}
