package com.squedgy.mcmodmanager.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import static org.slf4j.LoggerFactory.getLogger;

public class NewModsController {

	private static final Logger log = getLogger(NewModsController.class);
	@FXML
	public VBox root;
	@FXML
	public TabPane pane;

	private List<MenuButton> menuButtons = new LinkedList<>();

	public NewModsController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("new-mods.fxml"));
		loader.setController(this);
		loader.load();
		pane.maxHeightProperty().bind(root.heightProperty());
		pane.maxWidthProperty().bind(root.maxWidthProperty());

		buildTab("Curse Forge", new NewCurseForgeModController().getRoot(), t -> {
			t.closableProperty().setValue(false);
		});
	}

	public void buildTab(String tabTitle, Pane content, Consumer<Tab> modifier) {
		Tab button = new Tab(tabTitle);
		button.setText(tabTitle);
		button.setContent(content);
		content.prefHeightProperty().bind(pane.heightProperty());
		modifier.accept(button);
		pane.getTabs().add(button);
	}

	public VBox getRoot() {
		return root;
	}

}
