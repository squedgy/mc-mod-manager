package com.squedgy.mcmodmanager.controllers;

import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.request.VersionedRequest;
import com.squedgy.mcmodmanager.response.ModIdFoundConnectionFailed;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeResponse;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import org.slf4j.Logger;

import java.io.IOException;

import static com.squedgy.mcmodmanager.config.Configuration.getConfiguration;
import static org.slf4j.LoggerFactory.getLogger;

public class SetJarIdController {

	private static final Logger log = getLogger(SetJarIdController.class);
	@FXML
	private VBox root;
	private boolean updated;

	public SetJarIdController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("jar-ids.fxml"));
		loader.setController(this);
		loader.load();
		updated = false;
	}

	public boolean isUpdated() {
		return updated;
	}

	@FXML
	public void initialize() throws IOException {
		NewModUtils.getInstance().viewBadJars().forEach((id, reason) -> {
			if (!reason.equals(NewModUtils.NOT_FOUND_RESULT)) {
				ModInfo version = id.mod;

				if (version != null) {
					HBox holder = new HBox();

					Label label = new Label(version.getFileName() + ":");
					HBox.setMargin(label, new Insets(5, 5, 5, 5));
					label.fontProperty().setValue(new Font(16));
					TextInputControl input = new TextField(id.jarId);
					HBox.setMargin(input, new Insets(5, 5, 5, 5));
					Button curseForge = new Button("Try curse-forge id");
					Button saveAsDefault = new Button("Cache as-is");
					HBox.setMargin(curseForge, new Insets(5, 5, 5, 5));

					curseForge.onMouseReleasedProperty()
					          .setValue(e -> onMouseReleased(id, input, curseForge, holder, label));

					saveAsDefault.onMouseReleasedProperty()
					             .setValue(e -> {
					             	try {
					             		NewModUtils.getInstance()
						                           .addMod(id.jarId, id.mod, true);
					                } catch (IOException ex) {
						                log.error("Failed to write to cache.", ex);
					                }
					             });


					holder.getChildren().addAll(label, input, curseForge);

					root.getChildren().add(holder);
				}
			}
		});
	}

	private void onMouseReleased(NewModUtils.IdResult id, TextInputControl input, Button b, HBox holder, Label label) {
		NewModUtils utils = NewModUtils.getInstance();
		try {
			CurseForgeModDownloadInfo found = getLatestVersion(
				input.getText(),
				getConfiguration().getMinecraftVersion(),
				id.mod.getFileName()
			);
			if (found != null) {
				found.setRepositoryUniqueId(input.getText());
				utils.addMod(id.jarId, found, true);
				utils.cache.writeCache();
				holder.getChildren().setAll(label, new Label(input.getText()), new Label(" - success"));
				updated = true;
			} else {
				getIgnoreAndSaveButton(id, input, holder, label);
			}
		} catch (Exception ex) {
			log.error("An exception occurred while searching for {}", input.getText(), ex);
		}
	}

	private void getIgnoreAndSaveButton(NewModUtils.IdResult id, TextInputControl input, HBox holder, Label label) {
		NewModUtils utils = NewModUtils.getInstance();
		Button b = new Button();
		String givenModId = id.jarId;

		b.setText(String.format("Ignore Failure of '%s', and Save?", givenModId));
		id.mod.setRepositoryUniqueId(input.getText());
		b.onMouseReleasedProperty().setValue(e1 -> {
			try {
				utils.addMod(id.jarId, id.mod, true);
				holder.getChildren().setAll(label, new Label(input.getText()), new Label(" - saved"));
				updated = true;
			} catch (IOException e2) {
				log.error(e2.getMessage(), getClass());
			}
		});
	}

	private CurseForgeModDownloadInfo getLatestVersion(String id, String mcVersion, String fileName) throws
	                                                                                                 ModIdFoundConnectionFailed,
	                                                                                                 Exception {
		CurseForgeResponse cfr = CurseForgeRepository.getInstance().getForVersion(new VersionedRequest(id, mcVersion));
		return cfr.getRelatedVersions(Configuration.getConfiguration().getMinecraftVersion()).stream().filter(v -> v.getFileName().equals(fileName)).findFirst().orElse(null);
	}

	public VBox getRoot() {
		return root;
	}

}
