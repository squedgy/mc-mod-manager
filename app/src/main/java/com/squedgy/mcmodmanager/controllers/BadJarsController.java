package com.squedgy.mcmodmanager.controllers;

import com.squedgy.mcmodmanager.components.PublicNode;
import com.squedgy.mcmodmanager.display.JavafxUtils;
import com.squedgy.mcmodmanager.util.NewModUtils;
import com.squedgy.mcmodmanager.util.PathUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.io.File;
import java.io.IOException;
import java.util.stream.Collectors;

public class BadJarsController {

	@FXML
	private TableView<PublicNode> root;

	public BadJarsController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("bad-jars.fxml"));
		loader.setController(this);
		loader.load();
	}

	@FXML
	public void initialize() {
		root.setItems(FXCollections.observableArrayList(NewModUtils.getInstance().viewBadJars().entrySet().stream().map(PublicNode::new).collect(Collectors.toList())));
		TableColumn<PublicNode, String> one = JavafxUtils.makeColumn("File", v -> {
			try{
				String fileName = v.getValue().getKey().getFileName();
				return new SimpleStringProperty(fileName.contains(File.separator) ? fileName.substring((PathUtils.getModsDir() + File.separator).length()) : fileName);
			} catch (Exception e) {
				return new SimpleStringProperty("");
			}
		});
		TableColumn<PublicNode, String> two = JavafxUtils.makeColumn("Reason", v -> new SimpleStringProperty(v.getValue().getValue()));

		root.getColumns().setAll(
			one,
			two
		);
		root.refresh();
	}

	public TableView getRoot() {
		return root;
	}

}
