package com.squedgy.mcmodmanager;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.util.PathUtils;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.squedgy.mcmodmanager.util.PathUtils.allSubDirsMatch;
import static org.slf4j.LoggerFactory.getLogger;

public class Startup extends Application{
	private static Configuration CONFIG = Configuration.getConfiguration();


	public static void main(String[] args) {
		Font.loadFont(Startup.class.getResourceAsStream("FiraCode-Regular.otf"), 14);

		System.setProperty("sun.java2d.opengl", "true");
		String os = System.getProperty("os.name");

		Logger log = getLogger(Startup.class);

		String mcDir;
		//If custom set, otherwise looking for defaults
		if (CONFIG.getProperty(Configuration.CUSTOM_MC_DIR_KEY) != null) {
			mcDir = CONFIG.getProperty(Configuration.CUSTOM_MC_DIR_KEY);
		}
		else if (os.matches(".*[Ww]indows.*")) {
			mcDir = System.getenv("APPDATA") + File.separator + ".minecraft";
		}
		else if (os.matches(".*[Mm]ac [Oo][Ss].*")) {
			mcDir = System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support" + File.separator + "minecraft";
		}
		else mcDir = System.getProperty("user.home") + File.separator + ".minecraft";

		File dotMc = new File(mcDir);
		if (!dotMc.exists() || !dotMc.isDirectory() || !allSubDirsMatch(dotMc, "mods", "versions", "saves")) {
			mcDir = null;
		}
		PathUtils.setMinecraftDirectory(mcDir);

		if(!PathUtils.PROJECT_HOME.toFile().exists()){
			if(!PathUtils.PROJECT_HOME.toFile().mkdirs()) {
				log.error("Failed to startup, '{}' either doesn't exist and/or couldn't be created", PathUtils.PROJECT_HOME);
			}
		}

		launch(args);
	}

	public static void ensureMinecraftDirectory() {
		String minecraftDirectory = PathUtils.getMinecraftDirectory();
		while (minecraftDirectory == null) {
			DirectoryChooser chooser = new DirectoryChooser();
			chooser.setTitle("Choose Minecraft Dir");
			File location = chooser.showDialog(null);
			if(location == null){
				System.exit(0);
			}
			if (location.exists() && PathUtils.allSubDirsMatch(location, "mods", "versions", "saves")) {
				minecraftDirectory = location.getAbsolutePath();
				CONFIG.setProperty(Configuration.CUSTOM_MC_DIR_KEY, minecraftDirectory);
				CONFIG.writeProps();
			}
		}
		 PathUtils.setMinecraftDirectory(minecraftDirectory);
	}

	@Override
	public void start(Stage stage) throws IOException {

		App app = App.getInstance();
		stage.setTitle("Minecraft Mod Manager");
		stage.setMinHeight(app.getMainView().getRoot().getMinHeight());
		stage.setMinWidth(app.getMainView().getRoot().getMinWidth());
		stage.setScene(App.getParent());
		stage.onCloseRequestProperty().setValue(e -> {
			System.exit(0);
		});
		InputStream image = getClass().getResourceAsStream("minecraft-icon.png");
		stage.getIcons().setAll(new Image(image));
		stage.centerOnScreen();
		stage.show();
	}

}
