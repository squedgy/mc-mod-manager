package com.squedgy.mcmodmanager.components;

import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.display.ImageUtils;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import javafx.scene.image.ImageView;

import static com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository.REPOSITORY_ID;

public class DisplayVersion extends ModInfo {

	private String typeOfRelease;

	public DisplayVersion(ModInfo info) {
		super(info.getDisplayName(),
		      info.getRepositoryUniqueId(),
		      info.getFileName(),
		      info.getDownloadUrl(),
		      info.getDescription(),
		      info.getRepositoryId(),
		      info.getMinecraftVersion(),
		      info.getCreatedTime());
		typeOfRelease = info instanceof CurseForgeModDownloadInfo ? ((CurseForgeModDownloadInfo) info).getTypeOfRelease() : "";
	}

	public String getTypeOfRelease() { return typeOfRelease; }

	public ImageView getImage() {
		ImageUtils u = ImageUtils.getInstance();
		return new ImageView(NewModUtils.getInstance().modActive(this) ? u.GOOD : ImageUtils.getInstance().BAD);
	}

	public static CurseForgeModDownloadInfo toCurseInfo(DisplayVersion version) {
		if(version.getRepositoryId().equals(REPOSITORY_ID)){
			return new CurseForgeModDownloadInfo(
					version.getDisplayName(),
					version.getRepositoryUniqueId(),
					version.getDisplayName(),
					version.getDownloadUrl(),
					version.getDescription(),
					version.getMinecraftVersion(),
					version.getCreatedTime(),
					version.typeOfRelease
			);
		}
		return null;
	}

}
