package com.squedgy.mcmodmanager.components;

import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;

import java.util.Map;

public class PublicNode {

	private final ModInfo key;
	private final String value;

	public PublicNode(Map.Entry<NewModUtils.IdResult, String> entry) {
		key = entry.getKey().mod;
		value = entry.getValue();
	}

	public ModInfo getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "PublicNode{" +
			"key='" + key + '\'' +
			", value='" + value + '\'' +
			'}';
	}
}
