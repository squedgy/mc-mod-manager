package com.squedgy.mcmodmanager.display;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.web.WebView;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.slf4j.LoggerFactory.getLogger;

public class LoadingController {

	private static Logger log = getLogger(LoadingController.class);

	@FXML
	private WebView root;

	public LoadingController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("loading.fxml"));
		loader.setController(this);
		loader.load();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("loading.html")))) {
			String file = reader.lines().reduce("", (a,b) -> a + "\n" + b);
			root.getEngine().loadContent(file);
		} catch (Exception e) {
			log.error("Error reading loading.html", e);
			throw e;
		}
	}

	public WebView getRoot() {
		return root;
	}

}
