package com.squedgy.mcmodmanager.display;

import com.squedgy.mcmodmanager.App;
import com.squedgy.mcmodmanager.Startup;
import com.squedgy.mcmodmanager.components.DisplayVersion;
import com.squedgy.mcmodmanager.components.Modal;
import com.squedgy.mcmodmanager.controllers.*;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.threads.CurseForgeInfoThread;
import com.squedgy.mcmodmanager.threads.ModCheckingThread;
import com.squedgy.mcmodmanager.threads.ModLoadingThread;
import com.squedgy.mcmodmanager.util.NewModUtils;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.squedgy.mcmodmanager.display.ColumnManager.writeColumnOrder;
import static com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository.REPOSITORY_ID;
import static org.slf4j.LoggerFactory.getLogger;

public class MainController {

	private static final Logger log = getLogger(MainController.class);
	private static final String TABLE_NAME = "home";
	private static MainController instance;
	private ModVersionTableController table;
	@FXML
	private MenuItem badJars;
	@FXML
	private WebView objectView;
	@FXML
	private GridPane listGrid;
	@FXML
	private VBox root;
	@FXML
	private MenuBar menu;
	private boolean filled = false;
	private ModCheckingThread checking;

	private MainController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
		loader.setController(this);
		loader.load();
	}

	public static MainController getInstance() throws IOException {
		if(instance == null) {
			instance = new MainController();
		}
		return instance;
	}

	@FXML
	public void initialize() {
		Startup.ensureMinecraftDirectory();
		root.getStyleClass().add("dark");
		objectView.cacheProperty().setValue(true);
		objectView.getEngine().setJavaScriptEnabled(true);
		//Set the default view to a decent looking background
		updateObjectView("<h1>&nbsp;</h1>");
		loadMods();
	}

	public void loadMods() {
		Platform.runLater(() -> {
			try {
				LoadingController controller = new LoadingController();
				root.getChildren().setAll(controller.getRoot());

				controller.getRoot().prefWidthProperty().bind(root.widthProperty());
				controller.getRoot().prefHeightProperty().bind(root.heightProperty());
			} catch(IOException e) {
				log.error(e.getMessage(), getClass());
			}
		});

		ModLoadingThread t = new ModLoadingThread((mods) -> {
			Platform.runLater(() -> {
				try {
					initializeTable(mods);
				} catch(IOException e) {
					throw new RuntimeException(e);
				}
				if(!filled) {
					listGrid.add(table.getRoot(), 0, 0);
					filled = true;
				}
				badJars.setVisible(NewModUtils.getInstance().viewBadJars().size() > 0);
				root.getChildren().setAll(menu, listGrid);
				Platform.runLater(() -> App.getParent().getWindow().centerOnScreen());
			});
			return null;
		});
		t.start();
	}

	private void initializeTable(List<ModInfo> mods) throws IOException {
		if(table == null) {
			table = new ModVersionTableController(TABLE_NAME, mods.toArray(new ModInfo[0]));
		} else {
			ObservableList<DisplayVersion> observableList = FXCollections.observableArrayList(mods.stream()
			                                                                                      .map(DisplayVersion::new)
			                                                                                      .collect(Collectors.toList()));
			table.setItems(observableList);
		}
		if(!table.getColumns().get(0).getText().equals(" ")) {
			//Add the active/deactive image column here
			TableColumn<DisplayVersion, ImageView> col = new TableColumn<>();
			col.setText(" ");
			Callback<TableColumn.CellDataFeatures<DisplayVersion, ImageView>, ObservableValue<ImageView>> imageCellFactory = i -> new SimpleObjectProperty<>(
					i.getValue().getImage());
			col.setCellValueFactory(imageCellFactory);
			table.addColumn(0, col);
		}

		//The following two are made in code as
		//it could potentially not be necessary
		//for these to exist elsewhere
		//(De)activation toggling
		table.setOnDoubleClick(mod -> {
			NewModUtils utils = NewModUtils.getInstance();
			try {
				if(utils.modActive(mod)) {
					utils.deactivateMod(mod);
				} else {
					utils.activateMod(mod);
				}
			} catch(Exception e) {
				log.error("", e);
			}
			return null;
		});

		//Selection updating
		table.setOnChange((obs, old, neu) -> {
			if(neu == null) {
				updateObjectView("");
			} else if(neu.getDescription() == null && neu.getRepositoryId().equals(REPOSITORY_ID)) {
				updateObjectView("<h1>Loading...</h1>");
				CurseForgeModDownloadInfo info = new CurseForgeModDownloadInfo(neu.getDisplayName(),
				                                                               neu.getRepositoryUniqueId(),
				                                                               neu.getDisplayName(),
				                                                               neu.getDownloadUrl(),
				                                                               neu.getDescription(),
				                                                               neu.getMinecraftVersion(),
				                                                               neu.getCreatedTime(),
				                                                               neu.getTypeOfRelease());
				CurseForgeInfoThread gathering = new CurseForgeInfoThread(info,
				                                                          version -> Platform.runLater(() -> updateObjectView(
						                                                          version.getDescription())),
				                                                          () -> Platform.runLater(() -> updateObjectView(
						                                                          ("<h2>Error Loading, couldn't find a description!</h2>"))));
				JavafxUtils.putSetterAndStart(objectView, gathering);
			} else {
				updateObjectView(neu.getDescription());
			}
		});
	}

	public VBox getRoot() {
		return root;
	}

	public List<DisplayVersion> getItems() {
		return table.getItems();
	}

	public void setItems(List<ModInfo> mods) {
		table.setItems(FXCollections.observableArrayList(mods.stream()
		                                                     .map(DisplayVersion::new)
		                                                     .collect(Collectors.toList())));
	}

	private synchronized void updateObjectView(String description) {
		String style;
		try {
			List<BackgroundFill> backgroundFills = root.getBackground().getFills();
			BackgroundFill backgroundFill = backgroundFills.get(backgroundFills.size() - 1);
			String fill = backgroundFill.getFill().toString();

			Object value = listGrid.getCssMetaData()
			                   .stream()
			                   .map(e -> (CssMetaData<Styleable, ?>) e)
			                   .filter(f -> f.getProperty().equals("-fx-text-fill"))
			                   .findFirst()
			                   .map(e -> e.getInitialValue(root))
			                   .orElse(null);

			log.debug("ListGrid css meta-data");
			listGrid.getCssMetaData().forEach(meta -> log.debug("{}", meta));
			log.debug("ListGRid fx-text-fill: {}", value);

			if(fill.startsWith("0x")) { fill = fill.substring(2); }
			//		String style = String.format("body{background-color: %s; color: %s; margin-right: 20px;}" +
			//		                             "img{max-width:100%;height:auto; a{color:%s;text-decoration:none;}",
			//		                             fill,
			//		                             color);
		} catch(Exception e) {
			log.error("", e);
		}
		objectView.getEngine()
		          .loadContent("<style>" + "body{background-color:#303030; color:#ddd; margin-right: 20px;}" +
		                       "img{max-width:100%;height:auto;}" + "a{color:#ff9000;text-decoration:none;} " +
		                       "a:visited{color:#544316;}" + "</style>" + "<script>" +
		                       "window.onload = function() { var tags = document.getElementsByTagName('iframe'); for(var i = tags.length-1; i >= 0; i++){ tags[i].parentNode.removeChild(tags[i]); }}" +
		                       "</script>" + description);
	}

	@FXML
	public void setColumns(Event e) {
		writeColumnOrder(TABLE_NAME, table.getColumns());
	}

	public void updateModList() {
		setItems(NewModUtils.getInstance().getActiveMods());
	}

	@FXML
	public void updateList(Event e) {
		updateModList();
	}

	@FXML
	public void searchForUpdates(Event e) {
		Platform.runLater(() -> {
			Modal modal;
			try {

				modal = Modal.loading();
			} catch(IOException e1) {
				throw new RuntimeException();
			}

			if(checking == null || !checking.isAlive()) {

				checking = new ModCheckingThread(l -> {
					//do something with the returned list
					Platform.runLater(() -> {

						ModUpdaterController table;
						try {
							table = new ModUpdaterController(l.stream()
							                                  .filter(mod -> mod.getRepositoryId()
							                                                    .equals(REPOSITORY_ID))
							                                  .map(i -> (CurseForgeModDownloadInfo) i)
							                                  .collect(Collectors.toList()));
							modal.setContent(table.getRoot());
							modal.openAndWait(App.getParent().getWindow());
						} catch(IOException e1) {
							log.error("", e1);
							modal.close();
						}
					});
					return null;
				});
				checking.start();
			}
		});
	}

	@FXML
	public void showBadJars(Event e) {
		try {
			Modal m = Modal.getInstance(App.getParent().getWindow());
			BadJarsController c = new BadJarsController();
			m.setContent(c.getRoot());
			m.open(App.getParent().getWindow());
		} catch(IOException e1) {
			log.error(e1.getMessage(), getClass());
		}
	}

	@FXML
	public void setJarIds(Event e) {
		try {
			Modal m = Modal.getInstance(App.getParent().getWindow());
			SetJarIdController controller = new SetJarIdController();
			m.setContent(controller.getRoot());
			m.open(App.getParent().getWindow());
			m.setAfterClose(e2 -> {
				try {
					if(controller.isUpdated()) {
						App.getInstance()
						   .getMainView()
						   .getRoot()
						   .getChildren()
						   .setAll(new LoadingController().getRoot());
					}
					new Thread(() -> {
						if(controller.isUpdated()) {
							NewModUtils utils = NewModUtils.getInstance();
							log.debug("setting mods");
							utils.loadMods();
							loadMods();
						}
						m.setAfterClose(null);
					}).start();
				} catch(IOException e1) {
					log.error("", e1);
				}
			});
		} catch(IOException e1) {
			log.error(e1.getMessage());
		}

	}

	@FXML
	public void newMods(Event e) {
		try {
			Modal m = Modal.getInstance(App.getParent().getWindow());
			m.setContent(new NewModsController().getRoot());
			m.openAndWait(App.getParent().getWindow());
		} catch(IOException e1) {
			log.error(e1.getMessage(), getClass());
		}
	}

	@FXML
	public void mcVersion(Event e) {
		try {
			Modal modal = Modal.getInstance(App.getParent().getWindow());
			modal.setContent(new MinecraftVersionController().getRoot());
			modal.openAndWait(App.getParent().getWindow());
		} catch(IOException e1) {
			log.error("", e1);
		}
	}

}
