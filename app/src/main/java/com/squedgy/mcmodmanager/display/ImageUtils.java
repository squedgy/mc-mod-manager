package com.squedgy.mcmodmanager.display;

import javafx.scene.image.Image;

import java.io.InputStream;

public class ImageUtils {

	private static ImageUtils instance;
	public final Image GOOD, BAD;

	private ImageUtils() {
		GOOD = loadImage(getClass().getResourceAsStream("good.png"));
		BAD = loadImage(getClass().getResourceAsStream("bad.png"));
	}

	public static ImageUtils getInstance() {
		if (instance == null) instance = new ImageUtils();
		return instance;
	}

	public static Image loadImage(InputStream location) {
		return new Image(location);
	}

}
