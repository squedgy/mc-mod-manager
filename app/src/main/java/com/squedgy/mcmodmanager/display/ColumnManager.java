package com.squedgy.mcmodmanager.display;

import com.squedgy.mcmodmanager.components.DisplayVersion;
import com.squedgy.mcmodmanager.config.Configuration;
import javafx.scene.control.TableColumn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.squedgy.mcmodmanager.config.Configuration.getConfiguration;

public final class ColumnManager {

	private static final Configuration CONFIG = getConfiguration();

	private ColumnManager() {}

	public static int compareColumns(String tableName, String a, String b) {
		Integer one = null, two = null;
		try {
			one = Integer.valueOf(CONFIG.getProperty(tableName + "." + a));
		} catch (NumberFormatException ignore) {
		}
		try {
			two = Integer.valueOf(CONFIG.getProperty(tableName + "." + b));
		} catch (NumberFormatException ignore) {
		}
		if (one == null && two == null)
			return 0;
		else if (one == null)
			return -1;
		else if (two == null)
			return 1;

		return one.compareTo(two);
	}

	public static void writeColumnOrder(String tableName, List<TableColumn<DisplayVersion, ?>> order) {
		Map<String, String> props = new HashMap<>();
		for (int i = 0; i < order.size(); i++)
			props.put(order.get(i).getText(), String.valueOf(i));
		//Rewrite the columns keys to table.{column_name} so it's within an inner object
		props.forEach((key, value) -> CONFIG.setProperty(tableName + "." + key, value));
		//Add CONFIG so it's all nice and dandy
		CONFIG.writeProps();
	}

}
