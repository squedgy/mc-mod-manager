package com.squedgy.mcmodmanager.threads;

import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.request.VersionedRequest;
import com.squedgy.mcmodmanager.response.ModIdNotFoundException;
import com.squedgy.mcmodmanager.util.NewModUtils;
import javafx.util.Callback;
import org.slf4j.Logger;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository.REPOSITORY_ID;
import static org.slf4j.LoggerFactory.getLogger;

public class ModCheckingThread extends Thread {

	private static final Logger log = getLogger(ModCheckingThread.class);
	private final List<CurseForgeModDownloadInfo> IDS;
	private final String mc;
	private final List<CurseForgeModDownloadInfo> updateables = new LinkedList<>();
	private final Callback<List<CurseForgeModDownloadInfo>, ?> callback;
	private final Map<CurseForgeModDownloadInfo, Thread> updateCheckers;
	private static final Configuration CONFIG = Configuration.getConfiguration();

	public ModCheckingThread(Callback<List<CurseForgeModDownloadInfo>, ?> callback) {
		this(NewModUtils.getInstance()
		                .getActiveMods()
		                .stream()
		                .filter(e -> e.getRepositoryId().equals(REPOSITORY_ID))
		                .map(e -> (CurseForgeModDownloadInfo) e)
		                .collect(Collectors.toList()),
		     CONFIG.getMinecraftVersion(),
		     callback);
	}

	public ModCheckingThread(List<CurseForgeModDownloadInfo> modIds,
	                         String mcVersion,
	                         Callback<List<CurseForgeModDownloadInfo>, ?> callback) {
		IDS = new ArrayList<>(modIds);
		updateCheckers = new HashMap<>();
		this.mc = mcVersion;
		this.callback = callback;
	}

	private void removeThread(ModInfo v) {
		updateCheckers.remove(v);
	}

	@Override
	public void run() {
		IDS.forEach(id -> {
			Thread toRun = new Thread(() -> {
				try {
					CurseForgeModDownloadInfo resp;
					try {
						resp = CurseForgeRepository.getInstance().getLatest(new VersionedRequest(id.getRepositoryUniqueId(), mc));
					} catch(ModIdNotFoundException ignored) {
						resp = CurseForgeRepository.getInstance()
						                           .getLatest(new VersionedRequest(
							                           id.getDisplayName()
							                             .toLowerCase()
							                             .replace(' ', '-')
							                             .replaceAll("[^-a-z0-9]", ""),
							                           mc
						                           ));
					}
					String jarId = NewModUtils.getInstance().cache.findKey(e -> e.equals(id)).orElse(null);
					resp.setRepositoryUniqueId(jarId);

					if(resp.getCreatedTime().isAfter(id.getCreatedTime())) {
						updateables.add(resp);
					}
				} catch(ModIdNotFoundException | NullPointerException ignored) {
				} catch(Exception e) {
					log.error("", e);
				} finally {
					removeThread(id);
				}
			});
			updateCheckers.put(id, toRun);
			toRun.start();
		});
		while(updateCheckers.size() > 0) {
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch(InterruptedException e) {
				log.error("", e);
			}
		}
		callback.call(updateables);
	}

}
