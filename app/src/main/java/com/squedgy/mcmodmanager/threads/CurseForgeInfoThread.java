package com.squedgy.mcmodmanager.threads;

import com.squedgy.mcmodmanager.components.DisplayVersion;
import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeResponse;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.request.VersionedRequest;
import com.squedgy.mcmodmanager.response.ModIdFailedException;
import com.squedgy.mcmodmanager.util.NewModUtils;
import org.slf4j.Logger;

import java.util.function.Consumer;

import static org.slf4j.LoggerFactory.getLogger;

public class CurseForgeInfoThread extends Thread {

	private static final Logger log = getLogger(CurseForgeInfoThread.class);
	private final CurseForgeModDownloadInfo toFind;
	private final Consumer<CurseForgeModDownloadInfo> callback;
	private final Runnable couldntFind;

	public CurseForgeInfoThread(CurseForgeModDownloadInfo toFind, Consumer<CurseForgeModDownloadInfo> callback) {
		this(toFind, callback, null);
	}

	public CurseForgeInfoThread(CurseForgeModDownloadInfo toFind,
	                            Consumer<CurseForgeModDownloadInfo> callback,
	                            Runnable couldntFind) {
		this.couldntFind = couldntFind;
		this.toFind = toFind;
		this.callback = callback;
	}

	private void callback(CurseForgeModDownloadInfo v) {
		if(!Thread.currentThread().isInterrupted()) callback.accept(v);
	}

	private void failed() {
		if(couldntFind != null && !Thread.currentThread().isInterrupted()) couldntFind.run();
	}

	@Override
	public void run() {
		// if this isn't the right type, then the id has to be like... REALLY messed up
		CurseForgeModDownloadInfo ret =
			(CurseForgeModDownloadInfo) NewModUtils.getInstance().cache.getItem(toFind.getRepositoryUniqueId());
		if(ret != null) {
			callback(ret);
			return;
		}
		CurseForgeResponse resp = null;
		try {
			resp = CurseForgeRepository.getInstance()
			                           .getForVersion(new VersionedRequest(toFind.getRepositoryUniqueId(),
			                                                               toFind.getMinecraftVersion()
			                           ));
		} catch(ModIdFailedException e) {
			try {
				resp = CurseForgeRepository.getInstance()
				                           .getForVersion(new VersionedRequest(NewModUtils.formatModName(toFind.getDisplayName()),
				                                                               toFind.getMinecraftVersion()
				                           ));
			} catch(Exception e1) {
				log.error(e1.getMessage());
			}
		} catch(Exception e) {
			log.error(e.getMessage());
		}

		if(resp != null) {

			ret = resp.getRelatedVersions(Configuration.getConfiguration().getMinecraftVersion())
			          .stream()
			          .filter(e -> e.getFileName().equals(toFind.getFileName()))
			          .findFirst()
			          .orElse(null);
			if(ret != null) {
				callback(ret);
				return;
			}

		}
		failed();
	}

}
