package com.squedgy.mcmodmanager.threads;

import com.squedgy.mcmodmanager.App;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import javafx.util.Callback;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

public class ModLoadingThread extends Thread {

	private static final Logger log = getLogger(ModLoadingThread.class);
	private final Callback<List<ModInfo>, ?> callback;

	public ModLoadingThread(Callback<List<ModInfo>, ?> callback) {
		this.callback = callback;
	}

	@Override
	public void run() {
		log.info("Deciding minecraft version");
		App.decideMinecraftVersion();
		NewModUtils utils = NewModUtils.getInstance();
		utils.loadMods();
		ModInfo[] mods = utils.getActiveMods().toArray(new ModInfo[0]);
		List<ModInfo> modList = new LinkedList<>(Arrays.asList(mods));
		modList.addAll(utils.getInactiveMods());
		callback.call(modList);
	}
}
