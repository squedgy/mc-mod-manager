module app {
	requires org.slf4j;
	requires api;
	requires client;

	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.web;
	requires javafx.fxml;
	requires javafx.media;
	requires com.squedgy.utilities;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.core;
	requires ch.qos.logback.core;
	requires ch.qos.logback.classic;
	// Things that fxml needs to have access to
	opens com.squedgy.mcmodmanager.controllers to javafx.fxml;
	opens com.squedgy.mcmodmanager.components to javafx.fxml;
	opens com.squedgy.mcmodmanager.display to javafx.fxml;
	// the logback.xml file SHOULD live in this package
//	opens com.squedgy.mcmodmanager.logging to ch.qos.logback.classic, ch.qos.logback.core;

	exports com.squedgy.mcmodmanager;
}
