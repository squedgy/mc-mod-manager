module api {

	requires org.slf4j;
	requires com.fasterxml.jackson.core;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.annotation;
	requires com.fasterxml.jackson.datatype.jsr310;
	requires com.squedgy.utilities;
	requires java.net.http;
	requires jdk.jsobject;
	requires jdk.crypto.ec;
	requires classindex;

	exports com.squedgy.mcmodmanager.repository.curseforge;
	exports com.squedgy.mcmodmanager.repository.oneoff;
	exports com.squedgy.mcmodmanager.repository;
	exports com.squedgy.mcmodmanager.response.curseforge;
	exports com.squedgy.mcmodmanager.response;
	exports com.squedgy.mcmodmanager.request;
	exports com.squedgy.mcmodmanager.format;
	exports com.squedgy.mcmodmanager.analysers;

	opens com.squedgy.mcmodmanager.response.curseforge;
	opens com.squedgy.mcmodmanager.request;
}