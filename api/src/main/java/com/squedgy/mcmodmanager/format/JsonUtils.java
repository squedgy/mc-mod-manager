package com.squedgy.mcmodmanager.format;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public final class JsonUtils {

	private JsonUtils() {}

	public static boolean nodeHasAll(JsonNode n, String... subNodes) {
		for (String id : subNodes) if (!n.has(id)) return false;
		return true;
	}

	public static String getText(JsonNode node, String id) {
		return node.get(id).textValue();
	}

	public static long getLong(JsonNode node, String id) {
		return node.get(id).longValue();
	}

	public static boolean getBoolean(JsonNode node, String id) {
		return node.get(id).booleanValue();
	}

	public static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		return mapper;
	}
}
