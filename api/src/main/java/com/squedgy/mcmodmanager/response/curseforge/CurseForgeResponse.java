package com.squedgy.mcmodmanager.response.curseforge;

import java.util.List;
import java.util.Map;

public interface CurseForgeResponse {

	/**
	 * A list/reference of the creators/contributors
	 */
	public abstract List<ModMember> getMembers();
	/**
	 * The owner of the mod as dictated by the download site
	 */
	public abstract ModMember getOwner();
	/**
	 * A list of mod versions within the response from curseforge
	 */
	public abstract Map<String, List<CurseForgeModDownloadInfo>> getVersions();
	/**
	 * This should determine what {@link CurseForgeModDownloadInfo}'s match the given minecraft version
	 * @param mcVersion the minecraft version to get mod versions for
	 * @return a list of ModVersions for the given Minecraft version
	 */
	public abstract List<CurseForgeModDownloadInfo> getRelatedVersions(String mcVersion);
	/**
	 * Get the latest {@link CurseForgeModDownloadInfo} for the given Minecraft Version
	 * @param mcVersion the mincraft version to get the mod versions for
	 * @return The latest ModVersion for the given Minecraft version
	 */
	public abstract CurseForgeModDownloadInfo getLatestVersion(String mcVersion);

	public abstract String getTitle();
	// URL to their thumnail image (might be nice to implement something for this)
	public abstract String getThumbnail();
	public abstract String getDescription();

}
