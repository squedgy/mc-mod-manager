package com.squedgy.mcmodmanager.response.curseforge;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurseForgeModInfoResponse {

	protected String url, name,
			repositoryId, version,
			type;
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	protected Instant createdTime;

	public CurseForgeModInfoResponse() {
	}

	public CurseForgeModDownloadInfo toModInfo(String id, CurseForgeModInfo partOf) {
		return new CurseForgeModDownloadInfo(partOf.getTitle(),
		                                     id, name, url,
		                                     partOf.getDescription(), version,
		                                     createdTime, type);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Instant getCreatedTime() {
		return createdTime;
	}

	@JsonProperty("uploaded_at")
	public void setCreatedTime(Instant createdTime) {
		this.createdTime = createdTime;
	}
}
