package com.squedgy.mcmodmanager.response.curseforge;


public interface ModMember {

	public abstract String getTitle();

	public abstract String getUsername();

}
