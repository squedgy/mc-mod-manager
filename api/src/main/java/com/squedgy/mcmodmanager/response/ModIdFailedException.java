package com.squedgy.mcmodmanager.response;

public class ModIdFailedException extends RuntimeException {

	public ModIdFailedException() {
	}

	public ModIdFailedException(String mes) {
		super(mes);
	}

}
