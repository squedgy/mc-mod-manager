package com.squedgy.mcmodmanager.response.curseforge;

import java.time.Instant;

import com.fasterxml.jackson.annotation.*;
import com.squedgy.mcmodmanager.repository.ModInfo;

import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;
import static com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository.REPOSITORY_ID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurseForgeModDownloadInfo extends ModInfo {

	private String typeOfRelease;

	@JsonCreator(mode = PROPERTIES)
	public CurseForgeModDownloadInfo(@JsonProperty("irrelevant") String modName,
	                                 @JsonProperty("irrelevant2") String repositoryUniqueId,
	                                 @JsonProperty("name") String name,
	                                 @JsonProperty("url") String url,
	                                 @JsonProperty("description") String description,
	                                 @JsonProperty("version") String version,
	                                 @JsonFormat(shape = JsonFormat.Shape.STRING)
                                     @JsonProperty("uploaded_at") Instant uploadedAt,
	                                 @JsonProperty("type") String type) {
		super(
			modName,
			repositoryUniqueId,
			name,
			url,
			description,
			REPOSITORY_ID,
			version,
			uploadedAt
		);
		this.typeOfRelease = type;
	}

	public String getTypeOfRelease() {
		return typeOfRelease;
	}

	public void setTypeOfRelease(String typeOfRelease) {
		this.typeOfRelease = typeOfRelease;
	}

}
