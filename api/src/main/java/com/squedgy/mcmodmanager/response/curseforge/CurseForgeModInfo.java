package com.squedgy.mcmodmanager.response.curseforge;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squedgy.mcmodmanager.repository.ModInfo;
import org.slf4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurseForgeModInfo implements CurseForgeResponse {
	private static final Logger log = getLogger(CurseForgeModInfo.class);

	private String title;
	private List<Member> members;
	private Map<String, List<CurseForgeModInfoResponse>> versions;
	private Map<String, List<CurseForgeModDownloadInfo>> modVersions;
	// May want to track List of files
	// diet-hoppers has 1 file for multiple versions
	// see https://api.cfwidget.com/mc-mods/minecraft/278385-diet-hoppers for example of format
	// private List<CurseForgeFile> files
	// URL to their thumnail image (might be nice to implement something for this)
	private String thumbnail;
	private String description;

	public CurseForgeModInfo() {
	}

	public void afterCreation(String id) {
		modVersions = versions.entrySet()
				.stream()
				.map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue().stream().map(version -> version.toModInfo(id, this)).collect(toList())))
				.collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
	}

	@Override
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public List<ModMember> getMembers() {
		return Arrays.asList(members.toArray(new ModMember[0]));
	}

	@Override
	public ModMember getOwner() {
		return members.stream()
				.filter(member -> member.getTitle().equals("Owner"))
				.findFirst()
				.orElse(null);
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	@Override
	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Map<String, List<CurseForgeModDownloadInfo>> getVersions() {
		return modVersions;
	}

	@Override
	public List<CurseForgeModDownloadInfo> getRelatedVersions(String mcVersion) {
		String[] parts = mcVersion.split("\\.");
		List<CurseForgeModDownloadInfo> related = modVersions.get(mcVersion);
		if(parts.length > 2 && related == null) {
			related = modVersions.get(parts[0] + "." + parts[1]);
		}
		if(related == null) {
			// We may want to go through files here and search for matching minor/patch versions (see above by fields)
			return Collections.emptyList();
		}
		return related;
	}

	@Override
	public CurseForgeModDownloadInfo getLatestVersion(String mcVersion) {
		return getRelatedVersions(mcVersion)
				.stream()
				.max(Comparator.comparing(ModInfo::getCreatedTime))
				.orElse(null);
	}

	public void setVersions(Map<String, List<CurseForgeModInfoResponse>> versions) {
		this.versions = versions;
	}


}
