package com.squedgy.mcmodmanager.request;

public class VersionedRequest extends Request {
    public final String minecraftVersion;

    public VersionedRequest(String modId, String minecraftVersion) {
        super(modId);
        this.minecraftVersion = minecraftVersion;
    }

}