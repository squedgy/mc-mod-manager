package com.squedgy.mcmodmanager.request;

import java.net.URI;
import java.net.http.HttpRequest;

public class Request {
    public final String modId;

    public Request(String modId) {
        this.modId = modId;
    }

    public static HttpRequest getHttpRequest(URI uri) {
        return HttpRequest.newBuilder()
            .uri(uri)
            .GET()
            .headers(
                "User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0",
                "Accept", "*/*",
                "Accept-Language", "en-US,en;q=0.5",
                "Accept-Encoding", "gzip, deflate, br",
                "DNT", "1",
                "Upgrade-Insecure-Requests", "1"
            )
            .build();
    }

}