package com.squedgy.mcmodmanager.repository.curseforge;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.squedgy.mcmodmanager.repository.VersionedRepository;
import com.squedgy.mcmodmanager.request.Request;
import com.squedgy.mcmodmanager.request.VersionedRequest;
import com.squedgy.mcmodmanager.response.ModIdFoundConnectionFailed;
import com.squedgy.mcmodmanager.response.ModIdNotFoundException;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModInfo;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeResponse;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import static com.squedgy.mcmodmanager.format.JsonUtils.getObjectMapper;
import static com.squedgy.mcmodmanager.request.Request.getHttpRequest;
import static org.slf4j.LoggerFactory.getLogger;

public class CurseForgeRepository implements VersionedRepository<CurseForgeModDownloadInfo, CurseForgeResponse, Request, VersionedRequest, VersionedRequest> {

	private static CurseForgeRepository INSTANCE;

	private CurseForgeRepository() {}

	public static final CurseForgeRepository getInstance() {
		if(INSTANCE == null) INSTANCE = new CurseForgeRepository();
		return INSTANCE;
	}

	public static final String REPOSITORY_ID = "curseforge";

	private static final Logger log = getLogger(CurseForgeRepository.class);

    @Override
    public String getRepositoryId() {
        return REPOSITORY_ID;
    }

    private HttpResponse<InputStream> sendRequest(HttpClient client, HttpRequest request) throws IOException,
                                                                                                 InterruptedException,
                                                                                                 URISyntaxException {
    	log.debug("Sending request {}", request);
    	HttpResponse<InputStream> response = client.send(request, HttpResponse.BodyHandlers.ofInputStream());
    	while(response.statusCode() > 300 && response.statusCode() <= 399) {
    	    HttpRequest r = getHttpRequest(buildURI(response.headers().map().get("Location").get(0)));
		    log.debug("Request returned redirect, following to {}", r);
		    response = client.send(r, HttpResponse.BodyHandlers.ofInputStream());
	    }

	    log.debug("status: {}", response.statusCode());
    	return response;
    }

    @Override
    public InputStream download(CurseForgeModDownloadInfo v) {
		try{
			HttpClient client = HttpClient.newBuilder()
				.followRedirects(HttpClient.Redirect.NEVER)
				.cookieHandler(CookieHandler.getDefault())
				.build();

			HttpRequest request = getDownloadRequest(v);
			log.debug("trying to download mod {}", v.getRepositoryUniqueId());
			HttpResponse<InputStream> response = sendRequest(client, request);

			int statusCode = response.statusCode();
			if(statusCode < 200 || statusCode > 299) {
				request = getAlternativeDownloadRequest(v);
				response = sendRequest(client, request);
				statusCode = response.statusCode();
				if(statusCode < 200 || statusCode > 299) {
					throw new IOException(String.format("Received an HTTP %s from:\n%s", statusCode, response));
				}
			}

			if(response == null) {
				throw new IOException(
					String.format(
						"Couldn't connect to the web service at :%s",
						buildURI(v.getDownloadUrl() + "/file").toURL().toString()
					)
				);
			}

			return response.body();

		} catch (IOException | URISyntaxException | InterruptedException e) {
			log.error("There was an error requesting {}", v.getDownloadUrl() + "/file", e);
			return null;
		}
    }

    @Override
    public CurseForgeResponse get(Request info) throws Exception {
		return get(info.modId);
    }

    @Override
    public CurseForgeResponse getForVersion(VersionedRequest info) throws Exception {
		return get(info.modId);
    }

    private HttpResponse<InputStream> connectInOrderWithClient(HttpClient client, URI[] urls) {
    	for(URI uri : urls) {

		    HttpRequest get = HttpRequest.newBuilder(uri)
			    .GET()
			    .build();
		    try{
			    HttpResponse<InputStream> resp = client.send(get, HttpResponse.BodyHandlers.ofInputStream());

			    int responseCode = resp.statusCode();
			    log.debug("Response code: {}", responseCode);
			    if(responseCode >= 200 && responseCode < 300){
				    log.debug("returning response: {}", resp);
				    return resp;
			    }else {
				    log.trace("Non 200 response code: {}", resp);
			    }
		    }catch(IOException | InterruptedException e){
			    log.error(e.getMessage(), getClass());
		    }
	    }
    	return null;
    }

	private HttpResponse<InputStream> connectInOrder(URI... uris){
    	return connectInOrderWithClient(getClient(), uris);
	}

	private CurseForgeResponse get(String mod) throws IOException, URISyntaxException, ModIdFoundConnectionFailed {
		try{
			try {
				HttpResponse<InputStream> response = connectInOrder(
					buildURI("https://api.cfwidget.com/minecraft/mc-mods/" + mod),
					buildURI("https://api.cfwidget.com/mc-mods/minecraft/" + mod),
					buildURI("https://api.cfwidget.com/projects/" + mod));
				if(response == null){
					throw new IOException(String.format("%s couldn't be found/accessed", mod));
				}

				ObjectMapper mapper = getObjectMapper();
				CurseForgeModInfo info = mapper.readValue(response.body(), CurseForgeModInfo.class);
				info.afterCreation(mod);
				return info;
			} catch (FileNotFoundException e) {
				throw new IOException(e.getMessage());
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			} catch (Exception e) {
				throw new ModIdFoundConnectionFailed(String.format("Unknown Error with mod %s: %s", mod, e.getMessage()), e);
			}
		} catch(Exception e){ log.error("", e); throw e; }
	}

    @Override
    public CurseForgeModDownloadInfo getLatest(VersionedRequest info) {
		try {
			CurseForgeResponse resp = getForVersion(info);

			CurseForgeModDownloadInfo ret = resp
				.getRelatedVersions(info.minecraftVersion)
				.stream()
				.max(Comparator.comparing(CurseForgeModDownloadInfo::getCreatedTime))
				.orElse(null);
			Path p = Paths.get(System.getProperty("user.home") + File.separator + "checker-debug" + File.separator + info.modId + ".json");
			if(!p.toFile().exists()) {
				if(!p.getParent().toFile().exists())
					Files.createDirectories(p.getParent());
				Files.createFile(p);
			}
			getObjectMapper().writeValue(p.toFile(), resp);
			if (ret != null) return ret;
		} catch (Exception ex) {
			log.error("", ex);
		}
		throw new ModIdNotFoundException(
			String.format(
				"Couldn't find the mod Id : %s. It's doesn't match a Curse Forge mod. " +
				"Talk to the mod author about having the Id within their mcmod.info file match their Curse Forge mod id.",
				info.modId
			)
		);
    }

	private static URI buildURI(String url) throws URISyntaxException {
		int i = url.indexOf("://");
		String scheme = url.substring(0, i);
		int schemeOffset = i + "://".length();
		int slash = url.indexOf('/', schemeOffset);
		String host = url.substring(schemeOffset, slash);
		String path = url.substring(slash);
		// Things like Pam's Harvest Craft don't work without the following...
		// The first one encodes
		URI intermediate = new URI(scheme, host, path, null);
		// the second fixes any encoding encoded item issues
		String inbetween = intermediate.toString().replaceAll("%25([2-9A-Fa-f]{2})", "%$1");
		URI ret = new URI(inbetween);
		// Trace the result for safety
		log.trace("built url \"{}\"-> URI \"{}\" -> String \"{}\" to \"{}\"", url, intermediate, inbetween, ret);
		return ret;
	}

	private static HttpRequest getDownloadRequest(CurseForgeModDownloadInfo info) throws URISyntaxException {
		String downloadUrl = info.getDownloadUrl() + "/file";
		log.debug("Retrieved download url {}", downloadUrl);
		URI downloadUri = buildURI(downloadUrl);

		return getHttpRequest(downloadUri);
	}

	private static HttpRequest getAlternativeDownloadRequest(CurseForgeModDownloadInfo info) throws URISyntaxException {
    	String downloadUrl = info.getDownloadUrl().replace("/files/", "/download/") + "/file";
    	log.debug("Retrieved alternate download url {}", downloadUrl);
    	URI downloadUri = buildURI(downloadUrl);

    	return getHttpRequest(downloadUri);
	}

	private static HttpClient getClient(){
		HttpClient.Builder builder =  HttpClient.newBuilder()
			.followRedirects(HttpClient.Redirect.ALWAYS)
			.cookieHandler(CookieHandler.getDefault());

		//If proxy then add it
		if (System.getProperty("https.proxyPort") != null && System.getProperty("https.proxyHost") != null) {
			builder.proxy(ProxySelector.of(InetSocketAddress.createUnresolved(System.getProperty("https.proxyHost"), Integer.parseInt(System.getProperty("https.proxyPort")))));
		}
		return builder.build();
	}

}