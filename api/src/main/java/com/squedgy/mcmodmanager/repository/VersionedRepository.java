package com.squedgy.mcmodmanager.repository;

public interface VersionedRepository<DownloadInfo extends ModInfo, Response, Get, GetForVerison, GetLatest> extends ModRepository<DownloadInfo>{
    

    public abstract Response get(Get info) throws Exception;

    public abstract Response getForVersion(GetForVerison info) throws Exception;

    public abstract DownloadInfo getLatest(GetLatest info);
}
