package com.squedgy.mcmodmanager.repository.oneoff;

import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.repository.ModRepository;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.slf4j.LoggerFactory.getLogger;

public class SingleTimeDownload implements ModRepository<ModInfo> {

	public static final String REPOSITORY_ID = "one-time";

	private static Logger log = getLogger(SingleTimeDownload.class);

	@Override
	public InputStream download(ModInfo version) {
		HttpClient client = HttpClient.newBuilder()
			.followRedirects(HttpClient.Redirect.ALWAYS)
			.cookieHandler(CookieHandler.getDefault())
			.build();

		try {
			HttpRequest get = HttpRequest.newBuilder(new URI(version.getDownloadUrl()))
				.GET()
				.build();

			return client.send(get, HttpResponse.BodyHandlers.ofInputStream()).body();
		} catch (URISyntaxException e) {
			log.error("Error given link \"{}\" was invalid!", version.getDownloadUrl(), e);
		} catch (IOException | InterruptedException ex) {
			log.error("Error while creating client, or GETting '" + version.getDownloadUrl() + "'.", ex);
		}
		return null;
	}

	@Override
	public String getRepositoryId() {
		return REPOSITORY_ID;
	}

}