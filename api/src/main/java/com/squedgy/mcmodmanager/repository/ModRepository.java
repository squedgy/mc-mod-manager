package com.squedgy.mcmodmanager.repository;

import java.io.InputStream;

public interface ModRepository<DownloadInfo extends ModInfo> {

    public abstract String getRepositoryId();

    public abstract InputStream download(DownloadInfo version);

}