package com.squedgy.mcmodmanager.repository;

import org.atteo.classindex.IndexSubclasses;

import java.util.jar.JarFile;

@IndexSubclasses
public interface DownloadInfoAnalyser {

	/**
	 *
	 * @param file the jar file to analyze
	 * @return true if a "bad jar" false if not. Dictations of a "bad jar" are up to the
	 */
	boolean analyzeJar(JarFile file);

	String getRepositoryId();
}
