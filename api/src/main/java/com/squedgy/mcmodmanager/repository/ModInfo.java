package com.squedgy.mcmodmanager.repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeId;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.time.Instant;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
public class ModInfo implements Comparable<ModInfo> {

	protected String downloadUrl, fileName, repositoryId, description, repositoryUniqueId, displayName, minecraftVersion;
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	protected Instant createdTime;

	public ModInfo(String displayName,
	               String repositoryUniqueId,
	               String fileName,
	               String downloadUrl,
	               String description,
	               String repositoryId,
	               String minecraftVersion,
	               Instant createdTime) {
		this.displayName = displayName;
		this.repositoryUniqueId = repositoryUniqueId;
		this.fileName = fileName;
		this.downloadUrl = downloadUrl;
		this.description = description;
		this.repositoryId = repositoryId;
		this.minecraftVersion = minecraftVersion;
		this.createdTime = createdTime;
	}

	public ModInfo() {
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}

	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMinecraftVersion() {
		return minecraftVersion;
	}

	public void setMinecraftVersion(String minecraftVersion) {
		this.minecraftVersion = minecraftVersion;
	}

	public Instant getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Instant createdTime) {
		this.createdTime = createdTime;
	}

	public int compareTo(ModInfo o) {
		return o.getCreatedTime().compareTo(createdTime);
	}

	public String toString() {
		return "SimpleModInfo{" + "downloadUrl='" + downloadUrl + '\'' + ", fileName='" + fileName + '\'' +
		       ", repositoryId='" + repositoryId + '\'' + ", repositoryUniqueId='" + repositoryUniqueId + '\'' +
               ", displayName='" + displayName + '\'' + ", minecraftVersion='" + minecraftVersion + '\'' +
               ", createdTime=" + createdTime + '}';
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ModInfo)) return false;
		ModInfo that = (ModInfo) o;
		return Objects.equals(downloadUrl, that.downloadUrl) &&
				Objects.equals(fileName, that.fileName) &&
				Objects.equals(repositoryId, that.repositoryId) &&
				Objects.equals(description, that.description) &&
				Objects.equals(repositoryUniqueId, that.repositoryUniqueId) &&
				Objects.equals(displayName, that.displayName) &&
				Objects.equals(minecraftVersion, that.minecraftVersion) &&
				Objects.equals(createdTime, that.createdTime);
	}

	public int hashCode() {
		return Objects.hash(
				downloadUrl,
				fileName,
				repositoryId,
				description,
				repositoryUniqueId,
				displayName,
				minecraftVersion,
				createdTime
		);
	}

}