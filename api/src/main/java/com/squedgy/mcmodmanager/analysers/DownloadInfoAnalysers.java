package com.squedgy.mcmodmanager.analysers;

import com.squedgy.mcmodmanager.repository.DownloadInfoAnalyser;
import org.atteo.classindex.ClassIndex;
import org.slf4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;

import static org.slf4j.LoggerFactory.getLogger;

public final class DownloadInfoAnalysers {
	private DownloadInfoAnalysers() {}
	private static final DownloadInfoAnalyser alwaysGoodJar = new DownloadInfoAnalyser() {
		@Override
		public boolean analyzeJar(JarFile file) { return false; }
		@Override
		public String getRepositoryId() { return null; }
	};

	private static final Logger log = getLogger(DownloadInfoAnalysers.class);

	public static List<DownloadInfoAnalyser> getAnalysers() {
		return analysers;
	}

	public static DownloadInfoAnalyser getAnalyserForRepository(String repositoryId) {
		return analysers.stream()
			.filter(e -> e.getRepositoryId().equals(repositoryId))
			.findFirst()
			.orElse(alwaysGoodJar);
	}

	private static final List<DownloadInfoAnalyser> analysers = new LinkedList<>();

	private static void findAnalysers() {
		ClassIndex.getSubclasses(DownloadInfoAnalyser.class)
		          .forEach(clazz -> {
			          try {
				          Constructor<? extends DownloadInfoAnalyser> constructor = clazz.getConstructor();
				          // if zero arg constructor is public
				          if((constructor.getModifiers() & Modifier.PUBLIC) != 0){
					          try {
						          analysers.add(constructor.newInstance());
					          } catch(InstantiationException | IllegalAccessException | InvocationTargetException e) {
					          	log.error("Failed to instantiate {}.", clazz, e);
					          }
				          }
			          } catch(NoSuchMethodException ignored) {
			          }
		          });
	}
}
