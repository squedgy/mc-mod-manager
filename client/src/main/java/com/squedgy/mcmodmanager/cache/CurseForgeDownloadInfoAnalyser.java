package com.squedgy.mcmodmanager.cache;

import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.repository.DownloadInfoAnalyser;
import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.request.Request;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeResponse;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import org.slf4j.Logger;

import java.util.List;
import java.util.jar.JarFile;

import static com.squedgy.mcmodmanager.config.Configuration.getConfiguration;
import static com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository.REPOSITORY_ID;
import static com.squedgy.mcmodmanager.util.NewModUtils.getInstance;
import static org.slf4j.LoggerFactory.getLogger;

public class CurseForgeDownloadInfoAnalyser implements DownloadInfoAnalyser {

	private static final Logger log = getLogger(CurseForgeDownloadInfoAnalyser.class);

	@Override
	public boolean analyzeJar(JarFile file) {
		NewModUtils utils = getInstance();
		Configuration configuration = getConfiguration();
		String modId = utils.getJarModId(file);
		ModInfo info = utils.cache.getItem(modId);
		if(info != null && info.getRepositoryId().equals(getRepositoryId())) {
			try {
				CurseForgeModDownloadInfo cfInfo = (CurseForgeModDownloadInfo) info;
				CurseForgeResponse response = CurseForgeRepository.getInstance().get(new Request(cfInfo.getRepositoryUniqueId()));
				List<CurseForgeModDownloadInfo> versions =
					response.getRelatedVersions(configuration.getMinecraftVersion());
				return versions.stream().anyMatch(e -> e.getFileName().equals(file.getName()));
			} catch(Exception e) {
				log.error("Failed to get mod: {}", modId, e);
			}
		}
		return true;
	}

	@Override
	public String getRepositoryId() {
		return REPOSITORY_ID;
	}

}
