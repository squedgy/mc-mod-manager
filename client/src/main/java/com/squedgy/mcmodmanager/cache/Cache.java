package com.squedgy.mcmodmanager.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.squedgy.mcmodmanager.repository.ModInfo;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.squedgy.mcmodmanager.format.JsonUtils.getObjectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public class Cache {

	private static final Logger log = getLogger(Cache.class);
	private Map<String, ModInfo> cachedMods;
	private String fileName;

	private Cache(String file) {
		fileName = file;
		loadCache();
	}

	public static Cache reading(String file) {
		return new Cache(file);
	}

	public synchronized void loadCache() {
		try {
			log.info("loadCache called for file {}", fileName);
			ObjectMapper mapper = getObjectMapper();
			File f = new File(fileName);
			log.info("Cache file {}", f);
			if(!f.exists()) {
				if(f.getParentFile().exists() || f.getParentFile().mkdirs()) Files.write(f.toPath().toAbsolutePath(), "{}".getBytes());
				else {
					log.info("failed to create {}", f);
				}
			}

			cachedMods = mapper.readValue(f, Output.class);
		} catch (FileNotFoundException e) {
			log.error("Couldn't find cache file {}", fileName, e);
		} catch (Exception e) {
			log.error("An unexpected error occured while loading the cache at {}", fileName, e);
		}
	}

	public synchronized void writeCache() throws IOException {
		File f = new File(fileName);
		ObjectMapper mapper = getObjectMapper();
		try {
			Output output = new Output();
			output.putAll(cachedMods);
			mapper.writeValue(f, output);
		} catch (FileNotFoundException e) {
			if ((f.getParentFile().exists() || f.getParentFile().mkdirs()) && f.createNewFile()) {
				mapper.writeValue(f, cachedMods);
			}
		}
	}

	public void clearUnmatched(Map<String, ModInfo> mods) {
		Set<String> keys = mods.keySet();
		List<ModInfo> version = new LinkedList<>(mods.values());
		if(cachedMods == null) loadCache();
		cachedMods = cachedMods.entrySet()
			.stream()
			.filter(e -> keys.contains(e.getKey()) && version.contains(e.getValue()))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public void putItem(String modId, ModInfo version) {
		cachedMods.put(modId, version);
	}

	public ModInfo getItem(String modId) {
		return cachedMods.get(modId);
	}

	public Optional<String> findKey(Predicate<ModInfo> check) {
		return cachedMods.entrySet()
		                 .stream()
			.filter(e -> check.test(e.getValue()))
			.findFirst()
			.map(Map.Entry::getKey);
	}

	public static class Output extends HashMap<String, ModInfo> { }
}
