package com.squedgy.mcmodmanager.thread;

import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

import static org.slf4j.LoggerFactory.getLogger;

public class ModLocatorThread extends Thread {

	private static final Logger log = getLogger(ModLocatorThread.class);
	private BiConsumer<String, ModInfo> found;
	private Runnable failedToLocate;
	private String fileName;
	private List<String> jarIds;
	private CurseForgeModDownloadInfo toReturn = null;
	private static final NewModUtils utils = NewModUtils.getInstance();

	public ModLocatorThread(String jarId, String fileName, BiConsumer<String, ModInfo> found, Runnable failedToLocate){
		this.found = found;
		this.failedToLocate = failedToLocate;
		this.jarIds = Collections.singletonList(jarId);
		this.fileName = fileName;
		this.setName(fileName);
	}

	public ModLocatorThread(List<String> jarIds, String fileName, BiConsumer<String, ModInfo> found, Runnable failedToLocate){
		this.found = found;
		this.failedToLocate = failedToLocate;
		this.jarIds = jarIds;
		this.fileName = fileName;
		this.setName(fileName);
	}

	@Override
	public void run() {
		ModInfo toReturn = null;
		String matchingJarId = null;
		List<Exception> errors = new LinkedList<>();
		try {
			for(String jarId : jarIds) {
				try {
					log.info("Looking for matches for: " + jarId, getClass());
					toReturn = utils.matchesExistingId(jarId, fileName);
					if (toReturn != null) {
						matchingJarId = jarId;
						log.info("Found a match for: " + jarId, getClass());
						return;
					}
				} catch (Exception e) {
					log.error(e.getMessage(), getClass());
					errors.add(e);
				}
			}
		} finally {
			this.interrupt();
			if(toReturn != null) found.accept(matchingJarId, toReturn);
			else {
				failedToLocate.run();
				log.debug("Attempted ids: {}", jarIds);
				log.debug("Errors produced: {}", errors);
			}
		}
	}
}
