package com.squedgy.mcmodmanager.thread;

@FunctionalInterface
public interface Callback<Accepts, Returns> {
	public abstract Returns call(Accepts thing);
}
