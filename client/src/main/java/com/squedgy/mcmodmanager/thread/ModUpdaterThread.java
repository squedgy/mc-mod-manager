package com.squedgy.mcmodmanager.thread;

import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.util.NewModUtils;
import com.squedgy.mcmodmanager.util.PathUtils;
import com.squedgy.mcmodmanager.util.Result;
import org.slf4j.Logger;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.slf4j.LoggerFactory.getLogger;


public class ModUpdaterThread extends Thread {

	private static final Logger log = getLogger(ModUpdaterThread.class);
	private final List<CurseForgeModDownloadInfo> updates;
	private final Callback<Map<CurseForgeModDownloadInfo, Result>, Void> callback;
	private final Map<CurseForgeModDownloadInfo, Thread> updateThreads = new HashMap<>();

	public ModUpdaterThread(List<CurseForgeModDownloadInfo> updates, Callback<Map<CurseForgeModDownloadInfo, Result>, Void> callback) {
		this.updates = updates;
		this.callback = callback;
	}

	@Override
	public void run() {
		Map<CurseForgeModDownloadInfo, Result> results = new HashMap<>();
		updates.forEach(update -> {
			updateThreads.put(update, buildThread(update, results));
			updateThreads.get(update).start();
		});

		while (updateThreads.size() > 0) {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				log.error("", e);
			}
		}

		callback.call(results);
	}

	private Thread buildThread(CurseForgeModDownloadInfo update, Map<CurseForgeModDownloadInfo, Result> results) {
		return new Thread(() -> {
			boolean downloaded = false;
			String failed = "failed: Couldn't download, check logs for more information";
			try (InputStream file = CurseForgeRepository.getInstance().download(update)){
				ModInfo oldMod = NewModUtils.getInstance().cache.getItem(update.getRepositoryUniqueId());
				String fileLocation = PathUtils.findModLocation(oldMod);
				Path newFile = Paths.get(fileLocation).getParent().resolve(update.getFileName());
				if(newFile.toFile().exists()) {
					failed = "failed: new file \"" + newFile.toAbsolutePath() + "\" already exists!";
				} 
				else if(file == null) failed = "failed: Download failed. check logs for more information.";
				else if (!new File(fileLocation).exists()) failed = "failed: file \"" + fileLocation + "\" doesn't exist!";
				else {
					try (
						FileOutputStream outFile = new FileOutputStream(new File(newFile.toAbsolutePath().toString()));
						ReadableByteChannel in = Channels.newChannel(file);
						FileChannel out = outFile.getChannel()
					) {
						out.transferFrom(in, 0, Long.MAX_VALUE);
						downloaded = true;
					}catch (IOException e) {
						log.error(e.getMessage(), getClass());
					}
				}
			} catch (IOException e) {
				log.error("", e);
			} finally {
				if (downloaded) results.put(update, new Result(true, "succeeded"));
				else results.put(update, new Result(false, failed));
				log.info("removed: " + updateThreads.remove(update));
			}
		});
	}
}
