package com.squedgy.mcmodmanager.util;

import com.squedgy.mcmodmanager.repository.ModInfo;
import org.slf4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import static com.squedgy.mcmodmanager.config.Configuration.getConfiguration;
import static org.slf4j.LoggerFactory.getLogger;


public final class PathUtils {

	private static final Logger log = getLogger(PathUtils.class);
	public static final Path PROJECT_HOME = getProjectHome();
	private static String minecraftDirectory;

	public static Path getProjectHome() {
		String systemEnvPath = System.getenv("MMM_PROJ_HOME");
		String propertyPath = System.getProperty("mc.mod.manager.home");
		Path projectHome = null;
		if(systemEnvPath != null) {
			projectHome = Paths.get(systemEnvPath);
		}
		if (isInvalidHomeDir(projectHome) && propertyPath != null) {
			projectHome = Paths.get(propertyPath);
		}
		if(isInvalidHomeDir(projectHome)) {
			projectHome = getUserHome().resolve(".mc-mod-man");
		}
		log.info("Project home {}", projectHome);
		return projectHome;
	}

	private static boolean isInvalidHomeDir(Path p) {
		return p == null || !Files.exists(p) || !Files.isDirectory(p);
	}

	public static boolean allSubDirsMatch(File dir, String... subDirs) {
		if (dir.exists() && dir.isDirectory()) {
			for (String s : subDirs) {
				File f = dir.toPath().resolve(s).toFile();
				if (!f.exists() || !f.isDirectory()) return false;
			}
		}

		return true;
	}

	public static String getMinecraftDirectory() {
		return minecraftDirectory;
	}

	public static void setMinecraftDirectory(String dir) {
		if(dir != null) {
			File f = new File(dir);
			if (f.exists() && f.isDirectory() && allSubDirsMatch(f, "mods", "versions", "resourcepacks")) {
				minecraftDirectory = dir;
			}
		}
	}

	public static String getModsDir() {
		return minecraftDirectory + File.separator + "mods";
	}

	public static String getStorageDir() {
		return minecraftDirectory + File.separator + getConfiguration().getMinecraftVersion();
	}

	public static String getModLocation(ModInfo v) {
		return getModsDir() + File.separator + v.getFileName();
	}

	public static String getModStorage(ModInfo v) {
		return getStorageDir() + File.separator + v.getFileName();
	}

	public static String findModLocation(ModInfo v) {
		File f = new File(getModLocation(v));
		if(!f.exists()) f = new File(getModStorage(v));
		if(!f.exists()) f = new File(getModsDir() + File.separator + v.getFileName().replace(' ', '+'));
		if(!f.exists()) f = new File(getStorageDir() + File.separator + v.getFileName().replace(' ', '+'));
		return f.getAbsolutePath();
	}

	public static File getProjectDir(){
		try {
			return new File(URLDecoder.decode(PathUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8")).toPath().getParent().toFile();
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), PathUtils.class);
			return null;
		}
	}

	public static Path getPathFromProjectDir(String path){
		File base = getProjectDir();
		if(base != null){
			return base.toPath().resolve(path);
		}
		return Paths.get(path);
	}


	public static URL getResource(String resource) {
		return Thread.currentThread().getContextClassLoader().getResource(resource);
	}

	public static URL getOustideLocalResource(String path) {
		File f = new File(path);
		if (!f.toPath().getParent().toFile().exists()) if (!f.toPath().getParent().toFile().mkdirs()) return null;
		try {
			return f.toURI().toURL();
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public static URL getHttpResource(String link) {
		try {
			return new URL(link);
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public static Set<String> getPossibleMinecraftVersions(){
		Set<String> returns = new HashSet<>();
		File[] versions = new File(getMinecraftDirectory() + File.separator + "versions").listFiles();
		if(versions != null) {
			for (File f : versions) {
				if(f.getName().matches("[0-9.]+-[Ff]orge.*")) returns.add(f.getName().substring(0, f.getName().indexOf('-')));
			}
		}
		return returns;
	}

	public static Path getUserHome() {
		return Paths.get(System.getProperty("user.home"));
	}

}
