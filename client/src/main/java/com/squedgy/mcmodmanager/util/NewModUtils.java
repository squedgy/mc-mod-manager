package com.squedgy.mcmodmanager.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squedgy.mcmodmanager.cache.Cache;
import com.squedgy.mcmodmanager.config.Configuration;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.mcmodmanager.repository.curseforge.CurseForgeRepository;
import com.squedgy.mcmodmanager.repository.oneoff.SingleTimeDownload;
import com.squedgy.mcmodmanager.request.VersionedRequest;
import com.squedgy.mcmodmanager.response.ModIdFoundConnectionFailed;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeModDownloadInfo;
import com.squedgy.mcmodmanager.response.curseforge.CurseForgeResponse;
import com.squedgy.mcmodmanager.thread.ModLocatorThread;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import static com.squedgy.mcmodmanager.format.JsonUtils.getObjectMapper;
import static com.squedgy.mcmodmanager.format.JsonUtils.getText;
import static org.slf4j.LoggerFactory.getLogger;

public final class NewModUtils {

	public static final String NOT_FOUND_RESULT = "No tested id's matched curse forge, assuming its a one-off download";
	private static final Logger log = getLogger(NewModUtils.class);
	private static NewModUtils instance;

	private final Map<IdResult, String> badJars = new HashMap<>();
	private final Map<String, ModInfo> activeMods = new HashMap<>();
	private final Map<String, ModInfo> inactiveMods = new HashMap<>();
	public Cache cache;
	public final Configuration CONFIG;

	private NewModUtils() {
		CONFIG = Configuration.getConfiguration();
		cache = CONFIG.getCurrentModCache();
	}

	public static NewModUtils getInstance() {
		if (instance == null) {
			instance = new NewModUtils();
		}
		return instance;
	}

	public void loadMods() {
		activeMods.clear();
		inactiveMods.clear();
		badJars.clear();
		List<Thread> runningThreads = new LinkedList<>();
		// Make sure we're referencing the newest version of the cache
		cache = CONFIG.getCurrentModCache();
		// I'm trusting startup to do its job when it comes to correctly resolved mc-dirs
		File minecraftDir = new File(PathUtils.getMinecraftDirectory());
		File activeModsDir = minecraftDir.toPath().resolve("mods").toFile();
		File inactiveModsDir = minecraftDir.toPath().resolve(CONFIG.getMinecraftVersion()).toFile();

		scanDirForMods(activeModsDir, true, runningThreads);
		if (inactiveModsDir.exists() || inactiveModsDir.mkdir())
			scanDirForMods(inactiveModsDir, false, runningThreads);

		waitForThreads(runningThreads);

		log.info("Mods scanned from dirs attempting to updated cache");
		try {
			Map<String, ModInfo> allMods = new HashMap<>();
			allMods.putAll(activeMods);
			allMods.putAll(inactiveMods);
			cache.clearUnmatched(allMods);
			cache.writeCache();
		} catch (IOException e) {
			log.debug("Failed to write cache.", e);
		} catch (Exception e) {
			log.error("An unexpected error occured", e);
		}
	}

	public void addMod(String id, ModInfo mod, boolean active) throws IOException {
		addMod(id, mod, "", active);
	}

	public void addMod(String id, ModInfo mod, String issue, boolean active) throws IOException {
		addMod(id, mod, issue, active, true);
	}

	public static String formatModName(String name) {
		name = name.toLowerCase().replace(' ', '-');
		if (name.contains(":")) {
			name = name.substring(0, name.indexOf(':'));
		}
		name = name.replaceAll("[^-a-z0-9]", "").replaceAll("([^-])([0-9]+)|([0-9]+)([^-])", "$1-$2");
		return name;
	}

	// This should only happen if it wasn't found in the cache
	public CurseForgeModDownloadInfo matchesExistingId(String id, String fileName) throws Exception {
		CurseForgeModDownloadInfo ret;

		CurseForgeResponse response = CurseForgeRepository.getInstance()
				.getForVersion(new VersionedRequest(
						id,
						CONFIG.getMinecraftVersion()
				));

		List<CurseForgeModDownloadInfo> versions = response.getRelatedVersions(CONFIG.getMinecraftVersion());

		log.trace("Versions: {}", versions);

		log.info("potential file matches: " + versions.stream()
				.map(CurseForgeModDownloadInfo::getFileName)
				.collect(Collectors.joining("|||")), getClass());
		ret = versions.stream()
				.filter(v -> fileName.equals(v.getFileName()))
				.findFirst()
				.orElseThrow(() -> new ModIdFoundConnectionFailed(id + " contained a working id, but there was no matching file"));
		return ret;
	}

	public Map<IdResult, String> viewBadJars() {
		return Map.copyOf(badJars);
	}

	public String getJarModId(JarFile jarFile) {
		return getJarModIds(jarFile).stream().findFirst().orElse(jarFile.getName());
	}

	public List<String> getJarModIds(JarFile jarFile) {
		ZipEntry mcmodInfo = getMcmodInfo(jarFile);
		return getPotentialModIds(jarFile, mcmodInfo);
	}

	public boolean modActive(ModInfo info) {
		return activeMods.containsValue(info);
	}

	public ModInfo getAnyModFromId(String id) {
		ModInfo info = activeMods.get(id);
		if (info == null) {
			info = activeMods.values()
					.stream()
					.filter(e -> e.getRepositoryId().equals(CurseForgeRepository.REPOSITORY_ID))
					.filter(e -> e.getRepositoryUniqueId().equals(id))
					.findFirst()
					.orElse(null);
		}
		if (info == null) {
			info = inactiveMods.get(id);
		}
		if (info == null) {
			info = inactiveMods.values()
					.stream()
					.filter(e -> e.getRepositoryId().equals(CurseForgeRepository.REPOSITORY_ID))
					.filter(e -> ((CurseForgeModDownloadInfo) e).getRepositoryUniqueId().equals(id))
					.findFirst()
					.orElse(null);
		}
		return info;
	}

	public void deactivateMod(ModInfo info) throws IOException {
		String key = activeMods.entrySet()
				.stream()
				.filter(f -> f.getValue().equals(info))
				.findFirst()
				.map(Map.Entry::getKey)
				.orElse(null);
		if (key == null) throw new IllegalArgumentException(String.format("%s is not an active mod", info));
		String storageDir = PathUtils.getModStorage(info);
		String activeDir = PathUtils.getModLocation(info);
		Files.move(Paths.get(activeDir), Paths.get(storageDir));
		ModInfo active = activeMods.remove(key);
		inactiveMods.put(key, active);
	}

	public void activateMod(ModInfo info) throws IOException {
		String key = inactiveMods.entrySet()
				.stream()
				.filter(f -> f.getValue().equals(info))
				.findFirst()
				.map(Map.Entry::getKey)
				.orElse(null);
		String storageDir = PathUtils.getModStorage(info);
		String activeDir = PathUtils.getModLocation(info);
		Files.move(Paths.get(storageDir), Paths.get(activeDir));
		ModInfo active = inactiveMods.remove(key);
		activeMods.put(key, active);
	}

	private void scanDirForMods(File directory, boolean active, List<Thread> threads) {
		for (File file : Objects.requireNonNull(directory.listFiles())) {
			if (file.isDirectory()) {
				log.debug("{} vs {}", directory, file);
				scanDirForMods(file, active, threads);
			} else if (file.getName().endsWith(".jar")) {
				Thread searchThread = new Thread(getSearchForFileMod(file, cache, active));
				threads.add(searchThread);
				searchThread.start();
			}
		}
	}

	private Runnable getSearchForFileMod(File file, Cache cache, boolean active) {
		String fileName = file.getName();
		JarFile jarFile = getJarFile(file, fileName);

		return () -> {
			String jarId = getJarModId(jarFile);
			// If it's in the cache awesome!
			if (cache.getItem(jarId) != null) {
				ModInfo info = cache.getItem(jarId);
				if (!info.getFileName().equals(fileName)) {
					log.info("Jar file '{}' matched jarIds with '{}'. Assuming '{}' is the preferred jar/version.", fileName, info.getFileName(), fileName);
					info.setFileName(fileName);
				}
				addLoadedMod(jarId, info, active);
			}
			// See if the file matches a loaded one
			Optional<String> cachedModId = getModIdFromJar(fileName);
			if (cachedModId.isPresent()) {
				ModInfo mod = cache.getItem(cachedModId.get());
				if (mod != null) {
					addLoadedMod(jarId, mod, active);
				}
			}
			if (jarFile != null) {
				try {
					List<String> testIds = getJarModIds(jarFile);
					ModLocatorThread locatorThread = new ModLocatorThread(testIds,
							fileName,
							getModFoundResult(jarId, active),
							getModNotFoundResult(jarId, fileName, active)
					);
					locatorThread.start();
					locatorThread.join();
				} catch (NullPointerException | InterruptedException e) {
					log.error("Failed to read jar file", e);
				}
			}
		};
	}

	private Runnable getModNotFoundResult(String id, String fileName, boolean active) {
		ModInfo mod = new ModInfo("",
				fileName,
				fileName,
				"",
				"",
				SingleTimeDownload.REPOSITORY_ID,
				CONFIG.getMinecraftVersion(),
				Instant.now());
		return () -> addLoadedMod(id, mod, NOT_FOUND_RESULT, active);
	}

	private BiConsumer<String, ModInfo> getModFoundResult(String jarId, boolean active) {
		return (id, mod) -> addLoadedMod(jarId, mod, active);
	}

	private List<String> getPotentialModIds(JarFile modJar, ZipEntry mcmodInfo) {
		List<String> ids = new LinkedList<>();
		if (mcmodInfo != null && !mcmodInfo.isDirectory()) {
			try {
				JsonNode root = getObjectMapper().readTree(modJar.getInputStream(mcmodInfo));

				if (root.has("modList")) {
					root = root.get("modList");
				}

				if (root.isArray()) {
					root.forEach(child -> readNodeForIds(child, ids));
				} else {
					readNodeForIds(root, ids);
				}
			} catch (IOException e) {
				log.info("Failed to read jar file '{}' mcmod.info", modJar.getName());
			}
		}
		return ids;
	}

	private void readNodeForIds(JsonNode node, List<String> ids) {
		String id = getText(node, "modid"), name = getText(node, "name");

		if (id != null) {
			ids.add(id);
		}
		if (name != null) {
			ids.add(formatModName(name));
		}
	}

	private ZipEntry getMcmodInfo(JarFile jarFile) {
		return jarFile.getEntry("mcmod.info");
	}

	private JarFile getJarFile(File jarFileFile, String fileName) {
		try {
			return new JarFile(jarFileFile);
		} catch (IOException e) {
			log.error("Couldn't load '{}' as a jar file.", fileName);
			return null;
		}
	}

	// Id is the first potential mod-id found within the jar file.
	// It is NOT always the same as the download info's (assuming the downloadInfo implementation has a modId)
	private void addLoadedMod(String id, ModInfo mod, boolean active) {
		addLoadedMod(id, mod, "", active);
	}

	private void addLoadedMod(String id, ModInfo mod, String issue, boolean active) {
		try {
			addMod(id, mod, issue, active, false);
		} catch (IOException e) {
			log.error("This shouldn't even happen...", e);
		}
	}

	private void addMod(String id, ModInfo mod, String issue, boolean active, boolean write) throws
			IOException {
		if (issue == null || issue.isEmpty()) {
			badJars.put(new IdResult(mod, id), issue);
		}
		log.debug("adding mod with jar id {}: {}", id, mod);
		if (id != null) {
			cache.putItem(id, mod);
			(active ? activeMods : inactiveMods).put(id, mod);

			if (write) {
				cache.writeCache();
			}
		}
	}

	private Optional<String> getModIdFromJar(String fileName) {
		return cache.findKey(e -> e.getFileName().equals(fileName));
	}

	private void waitForThreads(List<Thread> threads) {
		while (threads.stream().anyMatch(Thread::isAlive)) {
			try {
				TimeUnit.MILLISECONDS.sleep(250);
			} catch (InterruptedException ignored) {
			} catch (Exception e) {
				log.error("An unexpected error occured while waiting for mod threads.", e);
			}
		}
	}

	public List<ModInfo> getActiveMods() {
		return activeMods.values()
				.stream()
				.collect(Collectors.toUnmodifiableList());
	}

	public List<ModInfo> getInactiveMods() {
		return inactiveMods.values()
				.stream()
				.collect(Collectors.toUnmodifiableList());
	}

	public static class IdResult {

		public ModInfo mod;
		public String jarId;

		IdResult(ModInfo v, String id) {
			this.mod = v;
			this.jarId = id;
		}

	}

}
