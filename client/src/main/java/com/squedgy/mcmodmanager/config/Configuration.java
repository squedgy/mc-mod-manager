package com.squedgy.mcmodmanager.config;

import com.squedgy.mcmodmanager.cache.Cache;
import com.squedgy.mcmodmanager.format.JsonInputStreamFormat;
import com.squedgy.mcmodmanager.repository.ModInfo;
import com.squedgy.utilities.reader.FileReader;
import com.squedgy.utilities.writer.FileWriter;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static com.squedgy.mcmodmanager.util.PathUtils.getProjectHome;
import static org.slf4j.LoggerFactory.getLogger;

public class Configuration {
	private static final Logger log = getLogger(Configuration.class);

	public static final Path CACHE_DIRECTORY = getProjectHome().resolve("cache");
	public static final String CUSTOM_MC_DIR_KEY = "mc-dir";
	protected static final Path CONFIG_DIR = getProjectHome().resolve("config");
	protected static final Path CONFIG_FILE = CONFIG_DIR.resolve("manager.json");
//	protected static final String MINECRAFT_ENVIRONMENT_VARIABLE = "MMM_MC_VERSION";
	protected static final String MINECRAFT_VERSION_KEY = "mc-version";
	protected static final JsonInputStreamFormat format = new JsonInputStreamFormat();
	protected static final FileReader<Map<String, String>> READER = new FileReader<>(format);
	protected static final FileWriter<Map<String, String>> WRITER = new FileWriter<>(CONFIG_FILE.toFile().getAbsolutePath(), format, false);

	private static Configuration instance;
	private Map<String, String> PROPERTIES;
	private String minecraftVersion = null;

	private Configuration() {
		try {
			PROPERTIES = readProps();
			decideMinecraftVersion();
		} catch (IOException e) {
			log.error("Failed to read props from {}", CONFIG_FILE, e);
		}
		log.info("Config Properties: {}", PROPERTIES);
	}

	public Cache getCurrentModCache() {
		return getCache();
	}

	public static Configuration getConfiguration() {
		if(instance == null) instance = new Configuration();
		return instance;
	}

	private void decideMinecraftVersion() {
		String tempVersion;
		// Trust user passed values first, default to config, otherwise if it's magically set then cool
		if((tempVersion = System.getenv("")) != null) minecraftVersion = tempVersion;
		else if((tempVersion = System.getProperty("mc.mod.manager.version")) != null) minecraftVersion = tempVersion;
		else if(PROPERTIES.containsKey(MINECRAFT_VERSION_KEY)) minecraftVersion = PROPERTIES.get(MINECRAFT_VERSION_KEY);
		else PROPERTIES.put(MINECRAFT_VERSION_KEY, minecraftVersion);
		writeProps();
	}

	public String getMinecraftVersion() {
		if(minecraftVersion == null) decideMinecraftVersion();
		return minecraftVersion;
	}

	public void setMinecraftVersion(String version) {
		minecraftVersion = version;
		PROPERTIES.put(MINECRAFT_VERSION_KEY, minecraftVersion);
//		getConfiguration().setChacher(minecraftVersion);
	}

	public String getProperty(String propKey) {
		return PROPERTIES.get(propKey);
	}

	public String setProperty(String propKey, String value) {
		return PROPERTIES.put(propKey, value);
	}

	public void deleteProperty(String propKey) {
		PROPERTIES.remove(propKey);
	}

	public Cache getCache() {
		String fileName = CACHE_DIRECTORY.resolve(minecraftVersion + ".json")
			.toAbsolutePath()
			.toString();
		Cache cache = Cache.reading(fileName);
		return cache;
	}

	private Map<String, String> readProps() throws IOException {
		try {
			log.info("Config file location {}", CONFIG_FILE.toFile().getAbsolutePath());
			if(!CONFIG_FILE.toFile().exists()){
				try {
					Files.createDirectories(CONFIG_FILE.getParent());
					Files.createFile(CONFIG_FILE);
					Files.write(CONFIG_FILE, "{}".getBytes(Charset.forName("UTF-8")));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		} catch (Exception e) {
			log.error("Read Props failed", e);
			throw e;
		}
		return readProps(CONFIG_FILE.toAbsolutePath().toString());
	}

	private Map<String, String> readProps(String file) throws IOException {
		return READER.read(file);
	}

	public void writeProps() {
		writeProps(CONFIG_FILE.toAbsolutePath().toString(), PROPERTIES);
	}

	private void writeProps(String file, Map<String, String> config) {
		try {
			WRITER.setFileLocation(file);
			WRITER.write(config);
		} catch (IOException e) {
			log.error("The file \"{}\" couldn't be written to.", file, e);
		}
	}

}
