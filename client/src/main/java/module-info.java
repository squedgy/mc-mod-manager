module client {
	requires api;
	requires org.slf4j;
	requires com.squedgy.utilities;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.core;
	requires classindex;

	exports com.squedgy.mcmodmanager.util;
	exports com.squedgy.mcmodmanager.cache;
	exports com.squedgy.mcmodmanager.config;
	exports com.squedgy.mcmodmanager.thread;
}